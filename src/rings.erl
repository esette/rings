-module(rings).

-export([main/1,
         help/0,
         parse_args/1,
         version/0,
	create/2,
	info/3]).

-include("rings.hrl").

main(Args) ->
    case catch(run(Args)) of
        ok ->
            ok;
        Error ->
            io:format("Uncaught error in ~p: ~p\n", [?MODULE, Error])
    end.

run(["help"|RawCmds]) when RawCmds =/= [] ->
    ok = load_app(),
    Cmds = unabbreviate_command_names(RawCmds),
    Args = parse_args(Cmds),
    BaseConfig = init_config(Args),
    {_, _} = process_options(BaseConfig, Args),
    help(long, [list_to_atom(C) || C <- Cmds]);
run(["help"]) ->
    help();
run(["info"|_]) ->
    help();
run(["version"]) ->
    ok = load_app(),
    version();
run(RawArgs) ->
    ok = load_app(),
    Args = parse_args(RawArgs),
    ?DEBUG("Args ~p~n", [Args]),
    BaseConfig = init_config(Args),
    {BaseConfig1, Commands} = process_options(BaseConfig, Args),
    %% Make sure crypto is running
    case crypto:start() of
        ok -> ok;
        {error,{already_started,crypto}} -> ok
    end,
    CommandAtoms = [list_to_atom(C) || C <- Commands],
    process_commands(BaseConfig1, CommandAtoms).

load_app() ->
    ok = application:load(?MODULE).

init_config({Options, _NonOptArgs}) ->
    ?DEBUG("Options ~p ~p~n", [Options, proplists:get_all_values(verbose, Options) ]),
    Config = rings_config:new(),
    Config1 = set_log_level(Config, Options),
    rings_log:init(Config1),
    %% Set other options here later if needed
    BaseConfig = rings_config:base_config(Config1),
    ScriptName = filename:absname(escript:script_name()),
    BaseConfig1 = rings_config:set_xconf(BaseConfig, escript, ScriptName),
    {ok, Cwd} = file:get_cwd(),
    AbsCwd = filename:absname(Cwd),
    ?DEBUG("rings location: ~p~n~p~n~p~n", [ScriptName, Cwd, AbsCwd]),
    rings_config:set_xconf(BaseConfig1, base_dir, AbsCwd).

process_options(Config, {Options, NonOptArgs}) ->
    ok = show(Options, NonOptArgs),
    {Config1, RawCmds} = filter_flags(Config, NonOptArgs, []),
    {Config1, unabbreviate_command_names(RawCmds)}.

help() ->
    OptSpecList = option_spec_list(),
    getopt:usage(OptSpecList, "rings",
                 "[var=value,...] <command,...>",
                 [{"var=value", "rings global variables (e.g. force=1)"},
                  {"command", "Command to run (e.g. compile)"}]),
    ?CONSOLE(
       "Type 'rings help <CMD1> <CMD2>' for help on specific commands."
       "~n~n", []).


%%
%% Parse command line arguments using getopt and also filtering out any
%% key=value pairs. What's left is the list of commands to run
%%
parse_args(RawArgs) ->
    %% Parse getopt options
    OptSpecList = option_spec_list(),
    case getopt:parse(OptSpecList, RawArgs) of
        {ok, Args} ->
            Args;
        {error, {Reason, Data}} ->
            ?ERROR("~s ~p~n~n", [Reason, Data]),
            help()
    end.

version() ->
    {ok, Vsn} = application:get_key(rings, vsn),
    ?CONSOLE("rings ~s\n",
             [Vsn]).

%%
set_log_level(Config, Options) ->
    LogLevel = case proplists:get_all_values(verbose, Options) of
                   [] ->
                       rings_log:default_level();
                   Verbosities ->
                       lists:last(Verbosities)
               end,
    rings_config:set_global(Config, verbose, LogLevel).

%%
%% set global flag based on getopt option boolean value
%%
%% set_global_flag(Config, Options, Flag) ->
%%     Value = case proplists:get_bool(Flag, Options) of
%%                 true ->
%%                     "1";
%%                 false ->
%%                     "0"
%%             end,
%%     rings_config:set_global(Config, Flag, Value).

show(Opts, NonOptArgs) ->
    false = show(help, Opts, fun help/0),
    false = show(commands, Opts, fun commands/0),
    false = show(version, Opts, fun version/0),
    case NonOptArgs of
        [] ->
            ?ABORT("No command to run specified!~n",[]),
            help(),
	    erlang:halt();
        _ ->
            ok
    end.

show(O, Opts, F) ->
    case proplists:get_bool(O, Opts) of
        true ->
            F(),
	    erlang:halt();
        false ->
            false
    end.

commands() -> help(short, [list_to_atom(Command) || Command <- command_names()]).

option_spec_list() ->
    VerboseHelp = "Verbosity level (-v, -vv, -vvv, --verbose 3). Default: 0",
    [
     %% {Name, ShortOpt, LongOpt, ArgSpec, HelpMsg}
     {help,     $h, "help",     undefined, "Show the program options"},
     {commands, $c, "commands", undefined, "Show available commands"},
     {verbose,  $v, "verbose",  integer,   VerboseHelp},
     {version,  $V, "version",  undefined, "Show version information"},
     {config,   $C, "config",   string,    "Ringtool config file to use"}
    ].

%%
%% Seperate all commands (single-words) from flags (key=value) and store
%% values into the rings_config global storage.
%%
filter_flags(Config, [], Commands) ->
    {Config, lists:reverse(Commands)};
filter_flags(Config, [Item | Rest], Commands) ->
    case string:tokens(Item, "=") of
        [Command] ->
            filter_flags(Config, Rest, [Command | Commands]);
        [KeyStr, RawValue] ->
            Key = list_to_atom(KeyStr),
            Value = case Key of
                        verbose ->
                            list_to_integer(RawValue);
                        _ ->
                            RawValue
                    end,
            Config1 = rings_config:set_global(Config, Key, Value),
            filter_flags(Config1, Rest, Commands);
        Other ->
            ?CONSOLE("Ignoring command line argument: ~p\n", [Other]),
            filter_flags(Config, Rest, Commands)
    end.

command_names() -> ["clean", "create", "destroy", "help", "list", "start", "stop"].

unabbreviate_command_names([]) ->
    [];
unabbreviate_command_names([Command | Commands]) ->
    case get_command_name_candidates(Command) of
        [] ->
            %% let the rest of the code detect that the command doesn't exist
            %% (this would perhaps be a good place to fail)
            [Command | unabbreviate_command_names(Commands)];
        [FullCommand] ->
            [FullCommand | unabbreviate_command_names(Commands)];
        Candidates ->
            ?ABORT("Found more than one match for abbreviated command name "
                   " '~s',~nplease be more specific. Possible candidates:~n"
                   "  ~s~n",
                   [Command, string:join(Candidates, ", ")])
    end.

get_command_name_candidates(Command) ->
    Candidates = [Candidate || Candidate <- command_names(),
                               is_command_name_candidate(Command, Candidate)],
    %% Is there a complete match?  If so return only that, return a
    %% list of candidates otherwise
    case lists:member(Command, Candidates) of
        true  -> [Command];
        false -> Candidates
    end.

is_command_name_candidate(Command, Candidate) ->
    lists:prefix(Command, Candidate)
        orelse is_command_name_sub_word_candidate(Command, Candidate).

is_command_name_sub_word_candidate(Command, Candidate) ->
    %% Allow for parts of commands to be abbreviated, i.e. create-app
    %% can be shortened to "create-a", "c-a" or "c-app" (but not
    %% "create-" since that would be ambiguous).
    ReOpts = [{return, list}],
    CommandSubWords = re:split(Command, "-", ReOpts),
    CandidateSubWords = re:split(Candidate, "-", ReOpts),
    is_command_name_sub_word_candidate_aux(CommandSubWords, CandidateSubWords).

is_command_name_sub_word_candidate_aux([CmdSW | CmdSWs],
                                       [CandSW | CandSWs]) ->
    lists:prefix(CmdSW, CandSW) andalso
        is_command_name_sub_word_candidate_aux(CmdSWs, CandSWs);
is_command_name_sub_word_candidate_aux([], []) ->
    true;
is_command_name_sub_word_candidate_aux(_CmdSWs, _CandSWs) ->
    false.

%% Given a list of modules, return a list containing the command to execute
select_modules([], _Command, _Arity, Acc) ->
    lists:reverse(Acc);
select_modules([Module | Rest], Command, Arity, Acc) ->
    {module, Module} = code:ensure_loaded(Module),
    case erlang:function_exported(Module, Command, Arity) of
        true ->
            select_modules(Rest, Command, Arity,  [Module | Acc]);
        false ->
            select_modules(Rest, Command, Arity, Acc)
    end.

%% Search all modules for help command
help(Type, Commands) ->
    {ok, AllModules} = application:get_key(?MODULE, modules),
    ?DEBUG("AllModules ~p~n", [AllModules]),
    lists:foreach(fun(Cmd) ->
			  ?DEBUG("==> help ~p~n~n", [Cmd]),
			  CmdModules = select_modules(AllModules, Cmd, 2, []),
			  Modules = select_modules(CmdModules, info, 3, []),
			  lists:foreach(fun(M) ->
						?DEBUG("=== ~p:~p ===~n", [M, Cmd]),
						case Type of
						    short ->
							%% format this
							{Args, Description} = M:info(help, Type, Cmd),
							?CONSOLE("~-30s ~s~n", [[atom_to_list(Cmd)|[" "|Args]], Description]);
						    long -> 
							%%M:info(help, Type, Cmd),
							?CONSOLE("~s~n", [M:info(help, Type, Cmd)])
						end
					end, Modules)
		  end, Commands).

%% Process each command in all selected modules
process_commands(Config, Commands) ->
    {ok, AllModules} = application:get_key(?MODULE, modules),
    ?DEBUG("AllModules ~p, Commands ~p~n", [AllModules, Commands]),
    lists:foreach(fun(Command) ->
			  Modules = select_modules(AllModules, Command, 2, []),
			  process_modules(Modules, Command, Config, undefined)
		  end, Commands).

process_modules([], _Command, Config, _Attrib) ->
    {ok, Config};
process_modules([Module | Rest], Command, Config, Attrib) ->
    case Module:Command(Config, Attrib) of
        ok ->
            process_modules(Rest, Command, Config, Attrib);
        {ok, NewConfig} ->
            process_modules(Rest, Command, NewConfig, Attrib);
        {error, _} = Error ->
            {Module, Error}
    end.

info(help, short, create) ->
    {"[config=mytopology]",  "Create a simulation ring based on topology file"};
info(help, long, create) ->
    "Create a simulation ring based on topology file \n\nValid command line options:  \n[config=mytopology]".

create(_Config, _Attrib) ->
    ?CONSOLE("create called~n", []).
