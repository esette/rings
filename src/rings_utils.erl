-module(rings_utils).

-export([cwd/0, rm/1,
	 rm_rf/1,
         cp_r/2,
         mv/2,
	 find_files/2, find_files/3,
	 escript_foldl/3,
         delete_each/1,
         write_file_if_contents_differ/2]).

-include("rings.hrl").

escript_foldl(Fun, Acc, File) ->
    case escript:extract(File, [compile_source]) of
        {ok, [_Shebang, _Comment, _EmuArgs, Body]} ->
            case Body of
                {source, BeamCode} ->
                    GetInfo = fun() -> file:read_file_info(File) end,
                    GetBin = fun() -> BeamCode end,
                    {ok, Fun(".", GetInfo, GetBin, Acc)};
                {beam, BeamCode} ->
                    GetInfo = fun() -> file:read_file_info(File) end,
                    GetBin = fun() -> BeamCode end,
                    {ok, Fun(".", GetInfo, GetBin, Acc)};
                {archive, ArchiveBin} ->
                    zip:foldl(Fun, Acc, {File, ArchiveBin})
            end;
        {error, _} = Error ->
            Error
    end.

find_files(Dir, Regex) ->
    find_files(Dir, Regex, true).

find_files(Dir, Regex, Recursive) ->
    filelib:fold_files(Dir, Regex, Recursive,
                       fun(F, Acc) -> [F | Acc] end, []).

cwd() ->
    {ok, Cwd} = file:get_cwd(),
    Cwd.

rm(Target) ->
    EscTarget = escape_spaces(Target),
    os:cmd(?FMT("rm -f~s", [EscTarget])).

rm_rf(Target) ->
    EscTarget = escape_spaces(Target),
    os:cmd(?FMT("rm -rf ~s", [EscTarget])).

cp_r([], _Dest) ->
    ok;
cp_r(Sources, Dest) ->
    EscSources = [escape_spaces(Src) || Src <- Sources],
    SourceStr = string:join(EscSources, " "),
    os:cmd(?FMT("cp -R ~s \"~s\"", [SourceStr, Dest])).

mv(Source, Dest) ->
    EscSource = escape_spaces(Source),
    EscDest = escape_spaces(Dest),
    os:cmd(?FMT("mv ~s ~s", [EscSource, EscDest])).

delete_each([]) ->
    ok;
delete_each([File | Rest]) ->
    case file:delete(File) of
        ok ->
            delete_each(Rest);
        {error, enoent} ->
            delete_each(Rest);
        {error, Reason} ->
            ?ERROR("Failed to delete file ~s: ~p\n", [File, Reason])
    end.

write_file_if_contents_differ(Filename, Bytes) ->
    ToWrite = iolist_to_binary(Bytes),
    case file:read_file(Filename) of
        {ok, ToWrite} ->
            ok;
        {ok,  _} ->
            file:write_file(Filename, ToWrite);
        {error,  _} ->
            file:write_file(Filename, ToWrite)
    end.


escape_spaces(Str) ->
    re:replace(Str, " ", "\\\\ ", [global, {return, list}]).
