-module(rings_log).

-include("rings.hrl").
-export([init/1,
         set_level/1, default_level/0,
         log/3]).

init(Config) ->
    Verbosity = rings_config:get_global(Config, verbose, default_level()),
    case valid_level(Verbosity) of
        0 -> set_level(error);
        1 -> set_level(warn);
        2 -> set_level(info);
        3 -> set_level(debug)
    end.

set_level(Level) ->
    ok = application:set_env(rings, log_level, Level).

log(Level, Str, Args) ->
    {ok, LogLevel} = application:get_env(rings, log_level),
    case should_log(LogLevel, Level) of
        true ->
            io:format(log_prefix(Level) ++ Str, Args);
        false ->
            ok
    end.

default_level() -> error_level().

valid_level(Level) ->
    erlang:max(error_level(), erlang:min(Level, debug_level())).

error_level() -> 0.
debug_level() -> 3.

should_log(debug, _)     -> true;
should_log(info, debug)  -> false;
should_log(info, _)      -> true;
should_log(warn, debug)  -> false;
should_log(warn, info)   -> false;
should_log(warn, _)      -> true;
should_log(error, error) -> true;
should_log(error, _)     -> false;
should_log(_, _)         -> false.

log_prefix(debug) -> "DEBUG: ";
log_prefix(info)  -> "INFO:  ";
log_prefix(warn)  -> "WARN:  ";
log_prefix(error) -> "ERROR: ".
