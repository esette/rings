-module(rings_config).

-export([new/0, new/1, base_config/1, consult_file/1,
         get/3, get_local/3, get_list/3,
         get_all/2,
         set/3,
         set_global/3, get_global/3,
         clean_config/2,
         set_xconf/3, get_xconf/2, get_xconf/3, erase_xconf/2]).

-include("rings.hrl").

-record(config, { dir :: file:filename(),
                  opts = [] :: list(),
                  globals = new_globals() :: dict(),
                  xconf = new_xconf() :: dict() }).

-export_type([config/0]).

-opaque config() :: #config{}.

-define(DEFAULT_NAME, "ring.config").

%% ===================================================================
%% Public API
%% ===================================================================

base_config(GlobalConfig) ->
    ConfName = get_global(GlobalConfig, config, ?DEFAULT_NAME),
    new(GlobalConfig, ConfName).

new() ->
    {ok, Dir} = file:get_cwd(),
    #config{dir = Dir}.

new(ConfigFile) when is_list(ConfigFile) ->
    case consult_file(ConfigFile) of
        {ok, Opts} ->
	    {ok, Dir} = file:get_cwd(),
            #config { dir = Dir,
                      opts = Opts };
        Other ->
            ?CONSOLE("Failed to load ~s: ~p~n", [ConfigFile, Other])
    end;
new(_ParentConfig=#config{opts=Opts0, globals=Globals,
                          xconf=Xconf}) ->
    new(#config{opts=Opts0, globals=Globals, xconf=Xconf},
        ?DEFAULT_NAME).

get(Config, Key, Default) ->
    proplists:get_value(Key, Config#config.opts, Default).

get_list(Config, Key, Default) ->
    get(Config, Key, Default).

get_local(Config, Key, Default) ->
    proplists:get_value(Key, local_opts(Config#config.opts, []), Default).

get_all(Config, Key) ->
    proplists:get_all_values(Key, Config#config.opts).

set(Config, Key, Value) ->
    Opts = proplists:delete(Key, Config#config.opts),
    Config#config { opts = [{Key, Value} | Opts] }.

set_global(Config, jobs=Key, Value) when is_list(Value) ->
    set_global(Config, Key, list_to_integer(Value));
set_global(Config, jobs=Key, Value) when is_integer(Value) ->
    NewGlobals = dict:store(Key, erlang:max(1, Value), Config#config.globals),
    Config#config{globals = NewGlobals};
set_global(Config, Key, Value) ->
    NewGlobals = dict:store(Key, Value, Config#config.globals),
    Config#config{globals = NewGlobals}.

get_global(Config, Key, Default) ->
    case dict:find(Key, Config#config.globals) of
        error ->
            Default;
        {ok, Value} ->
            Value
    end.

consult_file(File) ->
    case filename:extension(File) of
        ".script" ->
            consult_and_eval(remove_script_ext(File), File);
        _ ->
            Script = File ++ ".script",
            case filelib:is_regular(Script) of
                true ->
                    consult_and_eval(File, Script);
                false ->
                    ?DEBUG("Consult config file ~p~n", [File]),
                    file:consult(File)
            end
    end.

set_xconf(Config, Key, Value) ->
    NewXconf = dict:store(Key, Value, Config#config.xconf),
    Config#config{xconf=NewXconf}.

get_xconf(Config, Key) ->
    {ok, Value} = dict:find(Key, Config#config.xconf),
    Value.

get_xconf(Config, Key, Default) ->
    case dict:find(Key, Config#config.xconf) of
        error ->
            Default;
        {ok, Value} ->
            Value
    end.

erase_xconf(Config, Key) ->
    NewXconf = dict:erase(Key, Config#config.xconf),
    Config#config{xconf = NewXconf}.

%% TODO: reconsider after config inheritance removal/redesign
clean_config(Old, New) ->
    New#config{opts=Old#config.opts}.

%% ===================================================================
%% Internal functions
%% ===================================================================

new(ParentConfig, ConfName) ->
    {ok, Dir} = file:get_cwd(),
    ConfigFile = filename:join([Dir, ConfName]),
    Opts0 = ParentConfig#config.opts,
    Opts = case consult_file(ConfigFile) of
               {ok, Terms} ->
                   %% Found a config file with some terms. We need to
                   %% be able to distinguish between local definitions
                   %% (i.e. from the file in the cwd) and inherited
                   %% definitions. To accomplish this, we use a marker
                   %% in the proplist (since order matters) between
                   %% the new and old defs.
                   Terms ++ [local] ++
                       [Opt || Opt <- Opts0, Opt /= local];
               {error, enoent} ->
                   [local] ++
                       [Opt || Opt <- Opts0, Opt /= local];
               Other ->
                   ?CONSOLE("Failed to load ~s: ~p\n", [ConfigFile, Other])
           end,

    ParentConfig#config{dir = Dir, opts = Opts}.

consult_and_eval(File, Script) ->
    ?CONSOLE("Evaluating config script ~p~n", [Script]),
    ConfigData = try_consult(File),
    file:script(Script, bs([{'CONFIG', ConfigData}, {'SCRIPT', Script}])).

remove_script_ext(F) ->
    "tpircs." ++ Rev = lists:reverse(F),
    lists:reverse(Rev).

try_consult(File) ->
    case file:consult(File) of
        {ok, Terms} ->
            ?DEBUG("Consult config file ~p~n", [File]),
            Terms;
        {error, enoent} ->
            [];
        {error, Reason} ->
            ?CONSOLE("Failed to read config file ~s: ~p~n", [File, Reason])
    end.

bs(Vars) ->
    lists:foldl(fun({K,V}, Bs) ->
                        erl_eval:add_binding(K, V, Bs)
                end, erl_eval:new_bindings(), Vars).

local_opts([], Acc) ->
    lists:reverse(Acc);
local_opts([local | _Rest], Acc) ->
    lists:reverse(Acc);
local_opts([Item | Rest], Acc) ->
    local_opts(Rest, [Item | Acc]).

new_globals() -> dict:new().

new_xconf() -> dict:new().
